#!/bin/bash

top=$PWD

if [ ! -f env.sh ]; then
	echo Either env.sh was not found in the current directory $top.
	exit 1
fi

. ./env.sh

function setup_arch()
{
	a=$1
	cross=$a-linux

	case $a in
	i686)
		a=i386
		;;
	x86_64) 
		;;
	powerpc64)
		a=powerpc
		;;
	arm)
		cross=arm-linux-gnueabi
		;;
	arm64)
		a=arm64
		cross=aarch64-linux-gnu
		;;
	esac
	export PATH=$top/cross/ccache:$top/cross/bin/$cross/bin:$PATH
	opts="ARCH=$a CROSS_COMPILE=$cross-"
}

function setup_config()
{
	arch=$1
	conf=$2

	cd $top/media-git
	rm -rf ../trees/$conf/media-git
	mkdir ../trees/$conf/media-git
	make mrproper
	make $opts O=../trees/$conf/media-git allyesconfig
	cd ../trees/$conf/media-git
	if [ -f ../../../configs/$conf.config ]; then
		cp ../../../configs/$conf.config .config
	fi
	if [ $arch = x86_64 -o $arch = i686 ]; then
		perl -p -i -e 's/CONFIG_STAGING_EXCLUDE_BUILD=y/CONFIG_STAGING_EXCLUDE_BUILD=n/' .config
		perl -p -i -e 's/CONFIG_KCOV=y/CONFIG_KCOV=n/' .config
		perl -p -i -e 's/CONFIG_WERROR=y/CONFIG_WERROR=n/' .config
		perl -p -i -e 's/CONFIG_TOUCHSCREEN_HIDEEP=y/CONFIG_TOUCHSCREEN_HIDEEP=n/' .config
		perl -p -i -e 's/CONFIG_TOUCHSCREEN_EKTF2127=y/CONFIG_TOUCHSCREEN_EKTF2127=n/' .config
		perl -p -i -e 's/CONFIG_TOUCHSCREEN_ST1232=y/CONFIG_TOUCHSCREEN_ST1232=n/' .config
		perl -p -i -e 's/CONFIG_TOUCHSCREEN_ILI210X=y/CONFIG_TOUCHSCREEN_ILI210X=n/' .config
		perl -p -i -e 's/CONFIG_TOUCHSCREEN_STMPE=y/CONFIG_TOUCHSCREEN_STMPE=n/' .config
		( yes n | make $opts olddefconfig >/dev/null 2>&1 & ) ; sleep 5; kill `pidof yes` 2>/dev/null
		( yes | make $opts olddefconfig >/dev/null 2>&1 & ) ; sleep 5; kill `pidof yes` 2>/dev/null
	fi
	cd $top/media-git
	make $opts O=../trees/$conf/media-git prepare
}

function strip_top()
{
	sed "s;$top/media-git/;;g"
}

architectures="arm arm64 powerpc64 i686 x86_64"

if [ -z "$CPUS" ]; then
	CPUS=`cat /proc/cpuinfo | grep -i processor | wc -l`
fi
