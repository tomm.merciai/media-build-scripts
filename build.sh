#!/bin/bash

. ./funcs.sh || exit 1

cd $top

shopt -s nullglob

export LANG=C
export LC_ALL=C
export LANGUAGE=C
export LC_TIME=C
export PATH=/usr/local/bin:$PATH
kcflags="-Wmaybe-uninitialized"

daily=0
utils=0
misc=0
daily_test=0
no_pm=0
no_pm_sleep=0
no_of=0
no_acpi=0
no_debug_fs=0
sparse=0
smatch=0
spec=0
kerneldoc=0
virtme=0
virtme_arg=mc
virtme_32_arg=mc
virtme_quick=0
virtme_utils_path=$top/v4l-utils/build/usr/bin
virtme_test_media=$top/v4l-utils/contrib/test/test-media
parallel=1
specerr="NO_GIT"
logname=logs
cpus=$CPUS
branch=media_stage/master
patches=0

# For parallel builds use a fourth of the total number of CPUs
p_cpus=$(($CPUS/4))

sparse_git=git://git.kernel.org/pub/scm/devel/sparse/sparse.git
smatch_git=git://repo.or.cz/smatch.git

while [ ! -z "$1" ]; do
	case "$1" in
	-daily)
		daily=1
		misc=1
		no_pm=1
		no_pm_sleep=1
		no_of=1
		no_acpi=1
		no_debug_fs=1
		sparse=1
		smatch=1
		kerneldoc=1
		utils=1
		spec=1
		virtme_arg=all
		logname=daily-logs
		;;
	-daily-test)
		daily=1
		misc=1
		no_pm=1
		no_pm_sleep=1
		no_of=1
		no_acpi=1
		no_debug_fs=1
		sparse=1
		smatch=1
		kerneldoc=1
		utils=1
		spec=1
		virtme_arg=all
		daily_test=1
		;;
	-test)
		misc=1
		no_pm=1
		no_pm_sleep=1
		no_of=1
		no_acpi=1
		no_debug_fs=1
		sparse=1
		smatch=1
		kerneldoc=1
		spec=1
		;;
	-misc)
		misc=1
		;;
	-sparse)
		sparse=1
		;;
	-smatch)
		smatch=1
		;;
	-no-pm)
		no_pm=1
		;;
	-no-pm-sleep)
		no_pm_sleep=1
		;;
	-no-of)
		no_of=1
		;;
	-no-acpi)
		no_acpi=1
		;;
	-no-debug-fs)
		no_debug_fs=1
		;;
	-kerneldoc)
		kerneldoc=1
		;;
	-utils)
		utils=1
		;;
	-spec)
		spec=1
		;;
	-patches)
		shift
		patches=$1
		;;
	-virtme-arg)
		shift
		virtme_arg=$1
		;;
	-virtme-32-arg)
		shift
		virtme_32_arg=$1
		;;
	-virtme)
		virtme=1
		if [ ! -f v4l-utils/build/usr/bin/v4l2-compliance-32 ]; then
			utils=1
		fi
		;;
	-virtme-quick)
		virtme_quick=1
		;;
	-virtme-utils-path)
		shift
		virtme_utils_path="$1"
		;;
	-virtme-test-media)
		shift
		virtme_test_media="$1"
		;;
	-sequential)
		parallel=0
		;;
	-cpus)
		shift
		cpus=$1
		p_cpus=$(($cpus/4))
		if [ $p_cpus == 0 ]; then
			p_cpus=1
		fi
		if [ $cpus == 0 ]; then
			cpus=
			p_cpus=
		fi
		;;
	*)
		break
		;;
	esac
	shift
done

if [ "$1" == "clean" ]; then
	rm -rf history logs daily-logs media-git v4l-utils edid-decode sparse smatch virtme-git
	exit 0
fi

if [ "$1" == "setup" ]; then
	rm -rf history logs daily-logs media-git v4l-utils edid-decode sparse smatch virtme-git
	mkdir -p trees
	cd trees
	mkdir -p $architectures
	cd ..
	git clone git://linuxtv.org/v4l-utils.git
	git clone git://linuxtv.org/edid-decode.git
	git clone git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux-2.6.git media-git
	cd media-git
	git remote add media_tree git://linuxtv.org/media_tree.git
	git remote add media_stage git://linuxtv.org/media_stage.git
	git remote add main $myrepo
	if [ -n "$mylocalrepo" ]; then
		git remote add local $mylocalrepo
	fi
	git remote update
	git checkout -b build-test media_stage/master
	exit 0
fi

if [ -z "$1" ]; then
	echo 'Usage: build.sh [-patches N] [-test] [-misc] [-sparse] [-smatch]'
	echo '                [-no-pm] [-no-pm-sleep] [-no-of] [-no-acpi] [-no-debug-fs]'
	echo '                [-kerneldoc] [-spec] [-utils] [-sequential] [-cpus N]'
	echo '                [-virtme] [-virtme-arg ARG] [-virtme-32-arg ARG] [-virtme-quick]'
	#echo '                [-daily] [-daily-test]'
	echo '                arch|all|none [branch]'
	echo '       build.sh [clean|setup]'
	echo
	echo 'clean:  delete all logs, history and repositories.'
	echo 'setup:  does the same as clean and then sets up all the repositories'
	echo '        from scratch.'
	echo
	echo '-test: enables all options except -utils and -virtme/-virtme-all.'
	echo '-misc: enables several miscellaneous tests.'
	echo '-patches N: add the last N patches from the branch one by one and check if'
	echo '            it builds on x86_64 to prevent compile breakage.'
	echo '            Also run checkpatch over each patch. By default N=0.'
	echo '-no-pm: turns on the x86_64 CONFIG_PM=n build for the test run.'
	echo '-no-pm-sleep: turns on the x86_64 CONFIG_PM_SLEEP=n build for the test run.'
	echo '-no-of: turns on the x86_64 CONFIG_OF=n build for the test run.'
	echo '-no-acpi: turns on the x86_64 CONFIG_ACPI=n build for the test run.'
	echo '-no-debug-fs turns on the x86_64 CONFIG_DEBUG_FS=n build for the test run.'
	echo '-sparse: turns on the x86_64 sparse build for the test run.'
	echo '-smatch: turns on the x86_64 smatch build for the test run.'
	echo '-kerneldoc: checks kerneldoc of all media headers.'
	echo '-spec: build media specification docs.'
	echo '-utils: build v4l-utils and edid-decode.'
	echo '-virtme: build a virtme kernel and run the regression tests.'
	echo '-virtme-quick: skip building the virtme kernel, just run the regression tests.'
	echo '-virtme-arg: the argument to the test-media script (default is 'mc').'
	echo '-virtme-32-arg: the argument to the 32-bit test of test-media script (default is 'mc').'
	echo '-virtme-utils-path: where to find the v4l-utils utilities, by default use what -utils built.'
	echo '-virtme-test-media: the test-media script to run. By default use v4l-utils/contrib/test/test-media.'
	echo '-cpus N: use -jN for make when building git trees. If N is 0, then use -j only.'
	echo '         The parallel builds will use -j N/4. By default N is the number of'
	echo '         CPUs reported in /proc/cpuinfo.'
	echo '-sequential: build all architectures/configs sequentially instead of in parallel.'
	#echo '-daily: builds against all the specified architectures, builds all'
	#echo '        utilities, the special configs and the sparse/smatch builds and mails'
	#echo '        the results to the mailinglist.'
	#echo '-daily-test: does the same as -daily, except it sends the results to the builder only,'
	#echo '        not to the mailinglist.'
	echo
	echo 'arch|all|none: either specify a specific arch, do them all or do nothing.'
	echo
	echo 'Available architectures: '$architectures
	echo
	echo 'branch: the branch to use. Default is media_stage/master.'
	exit 1
fi

logdir=$top/$logname

if [ $parallel == 0 ]; then
	p_cpus=$cpus
fi

if [ "$1" = "all" ]; then
	archs="$architectures"
elif [ "$1" = "none" ]; then
	archs=""
else
	archs=$1
	if [ ! -d trees/$archs ]; then
		echo unknown arch $archs
		exit 1
	fi
fi
shift
if [ -n "$1" ]; then
	branch=$1
	if [[ $branch != */* ]]; then
		branch=main/$branch
	fi
	shift
fi

echo Building $branch for these architectures: $archs

rm -rf $logdir
mkdir -p $logdir

if [ $utils == 1 ]; then
	cd $top/v4l-utils
	git pull
	cd $top/edid-decode
	git pull
fi

cd $top/media-git
rm -f *.patch
rm -rf Documentation/output Documentation/media
git remote update
git reset --hard
git checkout master
git pull
git branch -D build-test
if [ $daily == 1 ]; then
	branch=media_stage/master
fi
git checkout -b build-test $branch || exit -1

DOCHDRS=include/uapi/linux/lirc.h
DOCHDRS="$DOCHDRS include/uapi/linux/videodev2.h"
DOCHDRS="$DOCHDRS include/uapi/linux/ivtv*.h"
DOCHDRS="$DOCHDRS include/uapi/linux/max2175.h"
DOCHDRS="$DOCHDRS include/uapi/linux/media*.h"
DOCHDRS="$DOCHDRS include/uapi/linux/v4l2*.h"
DOCHDRS="$DOCHDRS include/uapi/linux/uvcvideo.h"
DOCHDRS="$DOCHDRS include/uapi/linux/xilinx-v4l2-controls.h"
DOCHDRS="$DOCHDRS include/uapi/linux/ccs.h"
DOCHDRS="$DOCHDRS include/uapi/linux/smiapp.h"
DOCHDRS="$DOCHDRS include/uapi/linux/cec*.h"
DOCHDRS="$DOCHDRS "`find include/media`
DOCHDRS="$DOCHDRS "`find include/uapi/linux/dvb`
DOCHDRS="$DOCHDRS "`find drivers/media/|grep \\\.h$`
DOCHDRS="$DOCHDRS "`find drivers/staging/media/|grep -v atomisp|grep \\\.h$`


cd $top

if [ $smatch == 1 -o ! -d smatch ]; then
	if [ ! -d smatch ]; then
		git clone $smatch_git
	fi
	cd smatch
	git remote update
	git reset --hard origin/master
	make -j
	cd ..
fi
if [ $sparse == 1 -o ! -d sparse ]; then
	if [ ! -d sparse ]; then
		git clone $sparse_git
	fi
	cd sparse
	git remote update
	git reset --hard origin/master
	make -j
	cd ..
fi

export CCACHE_MAXSIZE=512M

logdate=`date +%Y%m%d-%H%M`.log
logday=`date +%A`

echo -n "date:			" >$logdir/summary
date >>$logdir/summary
echo "media-tree git repo:	$myrepo" >>$logdir/summary
echo "media-tree git branch:	$branch" >>$logdir/summary
cur_branch=`cd $top/media-git; git symbolic-ref --short -q HEAD`
cur_hash=`cd $top/media-git; git show-ref -s refs/heads/$cur_branch`
media_hash=$cur_hash
echo "media-tree git hash:	$cur_hash" >$logdir/hashes
cur_hash=`cd $top/v4l-utils; git show-ref -s refs/heads/master`
echo "v4l-utils git hash:	$cur_hash" >>$logdir/hashes
cur_hash=`cd $top/edid-decode; git show-ref -s refs/heads/master`
echo "edid-decode git hash:	$cur_hash" >>$logdir/hashes
echo -n "gcc version:		" >>$logdir/hashes
$top/cross/bin/i686-linux/bin/i686-linux-gcc --version | head -1 >>$logdir/hashes
echo    "sparse repo:            $sparse_git" >>$logdir/hashes
echo -n "sparse version:		" >>$logdir/hashes
$top/sparse/sparse --version >>$logdir/hashes
echo    "smatch repo:            $smatch_git" >>$logdir/hashes
echo -n "smatch version:		" >>$logdir/hashes
$top/smatch/smatch --version >>$logdir/hashes
echo    "build-scripts repo:     https://git.linuxtv.org/hverkuil/build-scripts.git" >>$logdir/hashes
cur_hash=`cd $top; git show-ref -s refs/heads/master`
echo    "build-scripts git hash: $cur_hash" >>$logdir/hashes
echo -n "host hardware:		" >>$logdir/hashes
uname -m >>$logdir/hashes
echo -n "host os:		" >>$logdir/hashes
uname -r >>$logdir/hashes
cat $logdir/hashes >>$logdir/summary
echo >>$logdir/summary
cp $logdir/summary $logdir/header

# Somehow this file gets generated occasionally. Make sure it is
# removed.
rm -f trees/Module.symvers
rm -f media-git/include/uapi/asm media-git/include/asm
rm -f $logdir/strcpy.log
touch $logdir/strcpy.log

cd $top/media-git

trap 'kill $(jobs -p) >/dev/null 2>&1' EXIT

if [ $patches -gt 0 ]; then
	git format-patch -M HEAD~$patches

	fail=0
	for p in *.patch; do
		if ! `grep -q "^Signed-off-by: $name <$email>" $p` ; then
			echo ERROR: $p: Missing Signed-off-by: "$name <$email>"
			fail=1
		fi
	done
	if [ $fail == 1 ]; then
		exit -1
	fi

	scripts/checkpatch.pl --strict *.patch | tee $logdir/checkpatch.log

	git reset --hard HEAD~$patches

	echo
	echo Sequentially build $patches patches...
	echo
	newconfig=1
	arch=x86_64
	export CCACHE_DIR=$top/trees/$arch/ccache-git
	mkdir -p $CCACHE_DIR
	for p in *.patch; do
		if [ $newconfig == 1 ]; then
			setup_config $arch $arch
		fi
		make $opts W=1 KCFLAGS="$kcflags" O=../trees/$arch/media-git -j$cpus drivers/staging/media/ drivers/media/ drivers/input/touchscreen/ || exit -1
		patches=$((patches-1))
		echo
		echo
		echo
		echo Test with patch $p
		echo
		echo
		echo
		git reset --hard $branch >/dev/null
		git reset --hard HEAD~$patches
		newconfig=0
		if egrep -q /Kconfig $p; then
			# Only recreate the kernel config if Kconfigs were touched
			# in the applied patch
			newconfig=1
		fi
		echo
		echo
		echo
		echo
	done

	for p in *.patch; do
		if ! `grep -q "^Subject:.*media:" $p` ; then
			echo WARNING: $p: Missing 'media:' prefix in Subject
		fi
	done
fi

for arch in $archs; do
	(
	cd $top/media-git

	setup_arch $arch

	echo
	echo
	echo
	echo Building for $arch...
	echo
	echo
	echo
	export CCACHE_DIR=$top/trees/$arch/ccache-git
	mkdir -p $CCACHE_DIR
	date >$logdir/linux-git-$arch.log
	setup_config $arch $arch
	cp ../trees/$arch/media-git/.config $logdir/$arch.config
	(make $opts W=1 KCFLAGS="$kcflags" O=../trees/$arch/media-git -j$p_cpus drivers/staging/media/ drivers/media/ drivers/input/touchscreen/ && echo End git build) 2>&1 | strip_top | tee -a $logdir/linux-git-$arch.log
	date >>$logdir/linux-git-$arch.log
	echo -n "linux-git-$arch: " >>$logdir/summary
	perl -f $top/parselog.pl <$logdir/linux-git-$arch.log >>$logdir/summary
	) &
	if [ $parallel == 0 ]; then
		wait
	fi
done
if [ $parallel == 1 ]; then
	wait
fi

configs=
if [ $no_pm -eq 1 ]; then
	configs="$configs no-pm"
fi
if [ $no_pm_sleep -eq 1 ]; then
	configs="$configs no-pm-sleep"
fi
if [ $no_of -eq 1 ]; then
	configs="$configs no-of"
fi
if [ $no_acpi -eq 1 ]; then
	configs="$configs no-acpi"
fi
if [ $no_debug_fs -eq 1 ]; then
	configs="$configs no-debug-fs"
fi

cd $top/media-git
setup_arch x86_64

for conf in $configs; do
	(
	echo
	echo
	echo
	echo arch x86_64 kernel git with $conf.config
	echo
	echo
	echo

	mkdir -p ../trees/$conf
	rm -rf ../trees/$conf/media-git
	mkdir ../trees/$conf/media-git
	export CCACHE_DIR=$top/trees/$conf/ccache-git
	mkdir -p $CCACHE_DIR

	date >$logdir/$conf.log
	make mrproper
	cp $top/configs/$conf.config ../trees/$conf/media-git/.config
	make $opts O=../trees/$conf/media-git olddefconfig
	make $opts O=../trees/$conf/media-git prepare
	cp ../trees/$conf/media-git/.config $logdir/$conf.config
	(make $opts O=../trees/$conf/media-git -j$p_cpus W=1 KCFLAGS="$kcflags" drivers/media/ drivers/staging/media/ && echo End git build) 2>&1 | strip_top | tee -a $logdir/$conf.log
	date >>$logdir/$conf.log
	echo -n "$conf.config: " >>$logdir/summary
	perl -f $top/parselog.pl <$logdir/$conf.log >>$logdir/summary
	) &
	if [ $parallel == 0 ]; then
		wait
	fi
done
if [ $parallel == 1 ]; then
	wait
fi

if [ $sparse -eq 1 ]; then
	echo
	echo
	echo
	echo arch i686 sparse build
	echo
	echo
	echo
	cd $top/media-git
	arch=i686
	conf=sparse
	setup_arch $arch
	export CCACHE_DIR=$top/trees/$conf/ccache-git
	mkdir -p $CCACHE_DIR
	date >$logdir/$conf.log
	setup_config $arch $conf
	(make $opts O=../trees/$conf/media-git -i -j$cpus W=1 C=1 CHECK=$top/sparse/sparse CF="-D__CHECK_ENDIAN__ -fmemcpy-max-count=11000000" KCFLAGS="$kcflags" drivers/media/ drivers/staging/media/ && echo End git build) 2>&1 | strip_top | tee -a $logdir/$conf.log
	date >>$logdir/$conf.log
	echo -n "$conf: " >>$logdir/summary
	perl -f $top/parselog.pl <$logdir/$conf.log >>$logdir/summary
fi

if [ $smatch -eq 1 ]; then
	echo
	echo
	echo
	echo arch x86_64 smatch build
	echo
	echo
	echo
	cd $top/media-git
	arch=x86_64
	conf=smatch
	setup_arch $arch
	export CCACHE_DIR=$top/trees/$conf/ccache-git
	mkdir -p $CCACHE_DIR
	date >$logdir/$conf.log
	setup_config $arch $conf
	(make $opts O=../trees/$conf/media-git -i -j$cpus W=1 C=1 CHECK=$top/smatch/smatch CF="-p=kernel" KCFLAGS="$kcflags" drivers/media/ drivers/staging/media/ && echo End git build) 2>&1 | strip_top | tee -a $logdir/$conf.log
	date >>$logdir/$conf.log
	echo -n "$conf: " >>$logdir/summary
	perl -f $top/parselog.pl <$logdir/$conf.log >>$logdir/summary
fi

cd $top/media-git

if [ $misc == 1 ]; then
	echo Check COMPILE_TEST...
	echo -n "COMPILE_TEST: " >>$logdir/summary
	fails=""
	make mrproper
	make ARCH=i386 allyesconfig >/dev/null && for i in  $(grep "config " $(find drivers/staging/media/ -name Kconfig) $(find drivers/media/ -name Kconfig) | grep -v "\#.*Kconfig" | grep -v MEDIA_HIDE_ANCILLARY_SUBDRV | grep -v VIDEO_TEGRA_TPG | cut -d' ' -f 2) ; do if [ "$(grep $i .config)" == "" ]; then fails="$fails $i"; fi; done
	if [ -z "$fails" ]; then
		echo OK >>$logdir/summary
	else
		echo WARNINGS:$fails >>$logdir/summary
	fi

	echo Check for strcpy/strncpy/strlcpy...
	echo -n "strcpy/strncpy/strlcpy: " >>$logdir/summary

	if git grep -q 'str[nl]\?cpy[^_]' drivers/staging/media/ drivers/media/ ; then
		git grep 'str[nl]\?cpy[^_]' drivers/staging/media/ drivers/media/ >$logdir/strcpy.log
		strcpy=`git grep 'strcpy[^_]' drivers/media/ drivers/staging/media/|wc -l`
		strlcpy=`git grep 'strlcpy[^_]' drivers/media/ drivers/staging/media/|wc -l`
		strncpy=`git grep 'strncpy[^_]' drivers/media/ drivers/staging/media/|wc -l`
		echo "WARNINGS: found $strcpy strcpy(), $strncpy strncpy(), $strlcpy strlcpy()" >>$logdir/summary
	else
		echo OK >>$logdir/summary
	fi

	cd $top

	echo Check for ABI changes...
	cat >test.c <<'EOF'
#include <linux/time.h>
#include <linux/string.h>
typedef unsigned long           uintptr_t;
#include <stdbool.h>
#include <stddef.h>
#include <linux/videodev2.h>
#include <linux/v4l2-subdev.h>
#include <linux/media.h>
/*#include <linux/cec.h>*/

union {
EOF
	#cat media-git/include/uapi/linux/videodev2.h media-git/include/uapi/linux/v4l2-subdev.h media-git/include/uapi/linux/media.h media-git/include/linux/cec.h | perl -ne 'if (m/\#define\s+([A-Z][A-Z0-9_]+)\s+\_IO.+\(.*\,\s*([^\)]+)\)/) {print "$2\n"}' | sort | uniq | perl -ne 'chomp; print "$_ t$num;\n"; $num++;' >>test.c
	cat media-git/include/uapi/linux/videodev2.h media-git/include/uapi/linux/v4l2-subdev.h media-git/include/uapi/linux/media.h | perl -ne 'if (m/\#define\s+([A-Z][A-Z0-9_]+)\s+\_IO.+\(.*\,\s*([^\)]+)\)/) {print "$2\n"}' | sort | uniq | perl -ne 'chomp; print "$_ t$num;\n"; $num++;' >>test.c
	echo '} x;' >>test.c
	( cd media-git/include/uapi; ln -sf asm-generic asm; cd ..; ln -sf asm-generic asm )

	rm -f $logdir/abi-dumper.log
	fail=false
	fail_result=""
	for arch in $archs; do
		setup_arch $arch
		$top/cross/bin/$cross/bin/$cross-gcc -g -Og -c test.c -D__KERNEL__ -I media-git/include/uapi -I trees/$arch/media-git/include/generated/uapi -I media-git/include
		abi-dumper -quiet test.o -o $logdir/abi.$arch.dump | strip_top | >>$logdir/abi-dumper.log 2>&1
		rm -f test.o
		if [ ! -s $logdir/abi.$arch.dump ]; then
			rm -f $logdir/abi.$arch.dump
		else
			if [ ! -f $top/abi/abi.$arch.dump ]; then
				cp $logdir/abi.$arch.dump $top/abi/abi.$arch.dump
			fi
			if ! abi-compliance-checker -l test.o -old abi/abi.$arch.dump -new abi/abi.$arch.dump; then
				fail_result="$fail_result $arch"
				false=true
			fi
			rm -rf compat_reports
		fi
	done
	if $fail; then
		echo abi-compliance: ABI WARNING: changed for$fail_result >>$logdir/summary
	else
		echo abi-compliance: ABI OK >>$logdir/summary
	fi
	rm -f test.c
	rm -f media-git/include/uapi/asm media-git/include/asm

	fail=false
	if ! ./pahole.sh ; then
		echo pahole: ABI ERROR: >>$logdir/summary
		fail=true
	else
		echo pahole: ABI OK >>$logdir/summary
	fi | tee $logdir/pahole.log
	if $fail; then
		echo >>$logdir/summary
		grep -v "^Install" $logdir/pahole.log >>$logdir/summary
		echo >>$logdir/summary
	fi
fi

# v4l-utils/edid-decode build
if [ $utils -eq 1 ]; then
	echo Build v4l-utils...
	date >$logdir/utils.log
	cd $top/v4l-utils
	git clean -f
	rm -rf build usr
	meson setup -Dv4l2-compliance-32=true -Dv4l2-ctl-32=true -Dprefix=$top/v4l-utils/build/usr -Dudevdir=$top/v4l-utils/build/usr/lib/udev -Dsystemdsystemunitdir=$top/v4l-utils/build/usr/lib/systemd build/
	ninja -C build/ | grep -v "strerrorname" | grep -v gethostbyname | strip_top | tee -a $logdir/utils.log
	ninja -C build install | strip_top | tee -a $logdir/utils.log

	echo Build edid-decode...
	cd $top/edid-decode
	git clean -f
	make clean >/dev/null 2>&1
	make -j$cpus edid-decode edid-decode.js 2>&1 | strip_top | tee -a $logdir/utils.log

	date >>$logdir/utils.log
	echo -n "utils: " >>$logdir/summary
	fail=true
	if grep -qi error $logdir/utils.log ; then
		echo ERRORS: >>$logdir/summary
	elif grep -qi warning $logdir/utils.log ; then
		echo WARNINGS: >>$logdir/summary
	else
		echo OK >>$logdir/summary
		fail=false
	fi
	if $fail; then
		echo >>$logdir/summary
		egrep '(warning)|(error)' -i $logdir/utils.log >>$logdir/summary
		echo >>$logdir/summary
	fi
fi

specerr="NO_GIT"

if [ $spec -eq 1 ]; then
	cd $top/media-git
	echo Build media spec...
	rm -rf Documentation/output Documentation/media
	sed s,userspace-api/media,media/userspace-api,g -i Documentation/Makefile
	mkdir -p Documentation/media
	cat <<END >Documentation/media/index.rst
.. SPDX-License-Identifier: GPL-2.0

.. include:: <isonum.txt>

**Copyright** |copy| 1991-: LinuxTV Developers

================================
Linux Kernel Media Documentation
================================

.. toctree::
	:maxdepth: 4

        userspace-api/index
        admin-guide/index
        driver-api/index
END

	rsync -vuza --delete Documentation/admin-guide/media/ Documentation/media/admin-guide
	rsync -vuza --delete Documentation/driver-api/media/ Documentation/media/driver-api
	rsync -vuza --delete Documentation/userspace-api/media/ Documentation/media/userspace-api
	date >$logdir/spec-git.log
	specerr="OK"
	fail=true
	make SPHINXDIRS="media" htmldocs 2>&1 | strip_top >>$logdir/spec-git.log
	date >>$logdir/spec-git.log
	git checkout Documentation/Makefile
	if grep -q 'ERROR:' $logdir/spec-git.log ; then
		specerr="ERRORS:"
	elif grep -q 'WARNING:' $logdir/spec-git.log ; then
		specerr="WARNINGS:"
	else
		fail=false
	fi
	echo "spec-git: $specerr" >>$logdir/summary
	if $fail; then
		echo >>$logdir/summary
		egrep '(WARNING:)|(ERROR:)' $logdir/spec-git.log >>$logdir/summary
		echo >>$logdir/summary
	fi
fi

if [ $kerneldoc -eq 1 ]; then
	cd $top/media-git
	echo Check kerneldoc...
	date >$logdir/kerneldoc.log
	for i in $DOCHDRS; do
		scripts/kernel-doc -none "$i" 2>&1 | strip_top >>$logdir/kerneldoc.log
	done
	date >>$logdir/kerneldoc.log
	echo -n "kerneldoc: " >>$logdir/summary
	fail=true
	if grep -qi error: $logdir/kerneldoc.log ; then
		echo ERRORS: >>$logdir/summary
	elif grep -qi warning: $logdir/kerneldoc.log ; then
		echo WARNINGS: >>$logdir/summary
	else
		echo OK >>$logdir/summary
		fail=false
	fi
	if $fail; then
		echo >>$logdir/summary
		egrep '(warning:)|(error:)' -i $logdir/kerneldoc.log >>$logdir/summary
		echo >>$logdir/summary
	fi
fi

if [ $virtme -eq 1 ]; then
	echo >>$logdir/summary
	echo -n "date:			" >>$logdir/summary
	date >>$logdir/summary

	# git virtme build
	cd $top/media-git
	setup_arch x86_64
	conf=virtme

	if [ $virtme_quick == 0 ]; then
		echo
		echo
		echo
		echo arch x86_64 kernel git with $conf.config
		echo
		echo
		echo

		mkdir -p ../trees/$conf
		rm -rf ../trees/$conf/media-git
		mkdir ../trees/$conf/media-git
		export CCACHE_DIR=$top/trees/$conf/ccache-git
		mkdir -p $CCACHE_DIR

		date >$logdir/$conf.log
		make mrproper
		cp $top/configs/$conf.config ../trees/$conf/media-git/.config
		make $opts O=../trees/$conf/media-git olddefconfig
		make $opts O=../trees/$conf/media-git prepare
		cp ../trees/$conf/media-git/.config $logdir/$conf.config
		make $opts O=../trees/$conf/media-git -j$cpus 2>&1 | strip_top | tee $logdir/$conf.log
		make -j$cpus O=../trees/$conf/media-git INSTALL_MOD_PATH=$top/trees/$conf/media-git/virtme-modules modules_install 2&>1 | strip_top | tee -a $logdir/$conf.log
		date >>$logdir/$conf.log
	fi

	cd ../trees/$conf/media-git
	rm -f $top/test-media-dmesg.log
	if [ -n "$virtme_arg" ]; then
		echo
		echo
		echo
		echo Running test-media -kmemleak $virtme_arg
		echo
		echo
		echo
		timeout --foreground -s INT 30m vng -r . -v --force-9p --cwd $top --rwdir $top -e "./virtme-test.sh "$virtme_utils_path" "$virtme_test_media" -kmemleak $virtme_arg" --user root --memory 4G --cpus 2 -o=-no-reboot 2>&1 | tee $logdir/$logday-test-media.log
		result=${PIPESTATUS[0]}
		if [ $result -eq 124 ]; then
			echo "qemu timed out!" >>$logdir/$logday-test-media.log
		fi
		if [ -f $top/test-media-dmesg.log ]; then
			mv $top/test-media-dmesg.log $logdir/$logday-test-media-dmesg.log
		fi

		echo -n "virtme: " >>$logdir/summary
		virtme_result=`grep "^Final " $logdir/$logday-test-media.log`
		if [ -z "$virtme_result" ]; then
			echo ERRORS >>$logdir/summary
		elif echo $virtme_result | grep -v "Failed: 0" ; then
			echo "ERRORS: $virtme_result" >>$logdir/summary
		elif echo $virtme_result | grep -v "Warnings: 0" ; then
			echo "WARNINGS: $virtme_result" >>$logdir/summary
		else
			echo "OK: $virtme_result" >>$logdir/summary
		fi
	fi
	if [ -n "$virtme_32_arg" ]; then
		echo
		echo
		echo
		echo Running test-media -kmemleak -32 $virtme_32_arg
		echo
		echo
		echo
		timeout --foreground -s INT 30m vng -r . -v --force-9p --cwd $top --rwdir $top -e "./virtme-test.sh "$virtme_utils_path" "$virtme_test_media" -kmemleak -32 $virtme_32_arg" --user root --memory 4G --cpus 2 -o=-no-reboot 2>&1 | tee $logdir/$logday-test-media-32.log
		result=${PIPESTATUS[0]}
		if [ $result -eq 124 ]; then
			echo "qemu timed out!" >>$logdir/$logday-test-media-32.log
		fi
		if [ -f $top/test-media-dmesg.log ]; then
			mv $top/test-media-dmesg.log $logdir/$logday-test-media-32-dmesg.log
		fi

		echo -n "virtme-32: " >>$logdir/summary
		virtme_result=`grep "^Final " $logdir/$logday-test-media-32.log`
		if [ -z "$virtme_result" ]; then
			echo ERRORS >>$logdir/summary
		elif echo $virtme_result | grep -v "Failed: 0" ; then
			echo "ERRORS: $virtme_result" >>$logdir/summary
		elif echo $virtme_result | grep -v "Warnings: 0" ; then
			echo "WARNINGS: $virtme_result" >>$logdir/summary
		else
			echo "OK: $virtme_result" >>$logdir/summary
		fi
	fi

fi

echo >>$logdir/summary
echo -n "date:			" >>$logdir/summary
date >>$logdir/summary

echo
echo
echo
echo ------------------------------------------------------
echo

cd $top

if grep -q "^[^ ]*: ERRORS" $logdir/summary ; then
	result=ERRORS
elif grep -q "^[^ ]*: ABI ERROR" $logdir/summary ; then
	result="ABI ERROR"
elif grep -q "^[^ ]*: ABI WARNING" $logdir/summary ; then
	result="ABI WARNING"
elif grep -q "^[^ ]*: WARNINGS" $logdir/summary ; then
	result=WARNINGS
else
	result=OK
fi

if [ $daily -eq 1 -a -f ./env-upload.sh ]; then
	. ./upload.sh
else
	cat $logdir/summary
	echo
	echo Overall build result: $result
	echo
fi
